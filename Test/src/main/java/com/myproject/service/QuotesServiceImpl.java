package com.myproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myproject.entity.Quotesentity;
import com.myproject.repository.Quotesrepo;

@Service
public class QuotesServiceImpl implements QuotesService{

	 @Autowired
	 private  Quotesrepo repo;

	    public List<Quotesentity> getAllEmployees() {
	        return (List<Quotesentity>) repo.findAll();
	    }
}
