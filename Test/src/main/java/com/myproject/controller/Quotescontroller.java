package com.myproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.myproject.service.QuotesService;

import ch.qos.logback.core.model.Model;

@Controller
@RequestMapping("/quotes")
public class Quotescontroller {

	@Autowired
	private QuotesService service;
	
	 @GetMapping
	    public String showQuotesPage() {
	        // This method just returns the initial view with the fetch button
	        return "index";
	    }

	 @GetMapping("/fetch")
	 public ModelAndView listEmployees() {
	        ModelAndView mav = new ModelAndView("index");
	        mav.addObject("quotes", service.getAllEmployees());
	        return mav;
	    }
}
