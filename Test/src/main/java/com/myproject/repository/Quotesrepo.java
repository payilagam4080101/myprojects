package com.myproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.myproject.entity.Quotesentity;

public interface Quotesrepo extends JpaRepository<Quotesentity,Integer>{

}
